export const state = () => ({
  slug: ''
})

export const mutations = {
  ADD_SLUG(state, req) {
    if (req) {
      let url = req.headers.host;
      var slug = "";
      if (url.includes("webkaar.com")) {
        slug = url.match(/(.*)\.webkaar\.com/)[1];
      } else {
        slug = url;
      }
      state.slug = slug;
    }
  }
}