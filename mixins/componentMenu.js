export default {
    props: {
        item: {
            type: Object
        },
        dashboardView: {
            type: Boolean,
            default: true
        },
    },
    methods: {
        openMenu(dashboardView, item) {
            if (dashboardView) {
                this.$emit("select:component", item);
            }
        }
    }
}
