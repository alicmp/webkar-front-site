export default {
  asyncData({ $axios, req, error, params, store }) {
    var slug = store.state.site.slug;
    if (!slug) {
      store.commit('site/ADD_SLUG', req);
      slug = store.state.site.slug;
    }

    if (params.page_slug) {
      var backend_url = `/website/show?website_slug=${encodeURIComponent(slug)}&page_slug=${encodeURIComponent(params.page_slug)}`;
    } else {
      var backend_url = `/website/show?website_slug=${encodeURIComponent(slug)}`;
    }

    return $axios.get(backend_url)
      .then((res) => {
        return {
          website: res.data
        }
      })
      .catch((e) => {
        error({ statusCode: e.response.status });
      })
  },
}
