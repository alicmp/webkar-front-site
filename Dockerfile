FROM node:12.16

ENV APP_ROOT /src

RUN mkdir ${APP_ROOT}
WORKDIR ${APP_ROOT}
ADD . ${APP_ROOT}

RUN npm install
RUN npm run build

ENV NUXT_HOST 0.0.0.0
ENV NUXT_PORT 3500