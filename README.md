# Webkar Site Front
Here is the code base for the webkar website builders front end. Besides this code base there are also
- webkar-api
- webkar-dashboard
## Requirements For Development Environment
- npm
## Requirements For Production Environment
- docker
## Projects Architecture
Each website is consists of various components such as Navbar, About, Footer, BlogFeed, and ... which are inside `components/website/views`.  
- The root of application is  `pages/index.html`.
- Inside it we load page info mixin `mixins/PageInfo.js` which is responsible for calling the api and getting information based on the websites URL.
- After that we load the `Website` component `components/website/views/Website.vue`, which is responsible for loading all of the components for that specific website dynamically and pass necessary information (such as text and images) to each of them.
- Each component has different layouts, which is specified by its name (layour_1, layout_2).
- This project also supports handling multi page websites. Check out `pages/_page_slug.vue` to learn how I handle it.
## Futurre Development
If you want to create a new component you can simply add it inside `components/website/views`. But be aware all components have to follow same architecture. You can check one of them to get the idea.
### .env file
create .env file and put
```
BASE_URL=https://app.webkaar.com/api/v1
frontURL=https://webkaar.com
```
in it for production. for development environment empty .env file will suffice.
### Initial Setup On development Environment
Since this project consists of two code base, one for dashboard project and one to handle showing each website, there are many files which are same in both code bases. In order to keep DRY principle you need to link these files from dashboard project here.  
Important: if you change one of these files in dashboard project dont forget to commit changes for Site project as well.
```bash
ln ~/Documents/workspace/mine/webkar/app-front/assets/sass/_font.scss ./assets/sass/_font.scss
ln ~/Documents/workspace/mine/webkar/app-front/assets/sass/_fontstyle.scss ./assets/sass/_fontstyle.scss
ln ~/Documents/workspace/mine/webkar/app-front/assets/sass/_formstyle.scss ./assets/sass/_formstyle.scss
ln ~/Documents/workspace/mine/webkar/app-front/assets/sass/_modalstyle.scss ./assets/sass/_modalstyle.scss
ln ~/Documents/workspace/mine/webkar/app-front/assets/sass/_styles.scss ./assets/sass/_styles.scss

ln ~/Documents/workspace/mine/webkar/app-front/components/website/views/* ./components/website/views/
```
### Production Setup
First you need to create bare git repository in the server. After that create `.git/hooks/post-receive` and add these information inside it:
```
#!/bin/bash

git --work-tree=/home/[USER]/app/site-front --git-dir=/home/[USER]/app/site-front/.git checkout -f
docker-compose -f /home/[USER]/app/site-front/docker-compose-prod.yml up -d --build
```
also make sure its executable `sudo chmod +x post-receive`
### Development Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```