
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'وبکار - وب‌سایت خودت رو بساز',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || 'خیلی آسون سایت خودت رو بساز و مشتری‌های خودت رو چند برابر کن!' }
    ],
    link: [
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  // loading: '~/components/utils/loading.vue',
  /*
  ** Global CSS
  */
  css: [
    '~/assets/sass/app.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    ["@nuxtjs/svg"]
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    'nuxt-material-design-icons'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  },
  publicRuntimeConfig: {
    axios: {
      baseURL: process.env.BASE_URL || 'http://localhost:8000/api/v1'
    },
    frontURL: process.env.frontURL
  },
}
